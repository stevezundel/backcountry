var gulp = require("gulp");
var gutil = require("gulp-util");
var webpack = require("webpack");
var WebpackDevServer = require("webpack-dev-server");
var config = require('./webpack.config');
var path = require('path');


gulp.task('webpack-dev-server', function (callback) {
    var WebpackDevServer = require('webpack-dev-server');

    var config = require('./webpack.config');
    var compiler = webpack(config);

    new WebpackDevServer(compiler, {
        hot: true,
        inline: true,
        historyApiFallback: true,
        proxy: {
            '*': 'http://localhost:3000',
        },
        quiet: false,
        noInfo: false,

        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        },

        stats: { colors: true },
    }).listen(8080, 'localhost', function (err) {
        if (err) throw new gutil.PluginError('webpack-dev-server', err);
        // Server listening
        gutil.log('[webpack-dev-server iframe]', 'http://localhost:8080/webpack-dev-server');
        gutil.log('[webpack-dev-server inline]', 'http://localhost:8080');

        // keep the server alive or continue?
        // callback();
    });
});