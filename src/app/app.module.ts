import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { AppComponent }         from './app.component';
import { LoginModule } from './login/login.module';
import { RouterModule }   from '@angular/router';
import { LocationStrategy, HashLocationStrategy} from '@angular/common';
import { HeroListComponent }    from './hero-list.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        LoginModule,
        RouterModule.forRoot([
            { path: '', component: HeroListComponent },
            { path: 'heroes', component: HeroListComponent }
        ])
    ],
    exports:[
        RouterModule
    ],
    declarations: [
        AppComponent,
        HeroListComponent
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy}
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule {
}
