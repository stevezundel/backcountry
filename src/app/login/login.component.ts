import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import {Http} from '@angular/http';
import 'rxjs/add/operator/map'
@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    private http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    onSubmit(){
        debugger;
        this.http.get('login').map(res => res).subscribe()
    }
}
